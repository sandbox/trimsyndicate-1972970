<?php

/**
 * @file
 * No Login module functions.
 */

/**
 * Implements hook_init().
 */
function no_login_init() {
  // Deny access to user/login, user/register, user/password.
  // Allow access to user/reset/* to allow drush uli to work.
  if (arg(0) == 'user') {
    switch (arg(1)) {
      case 'login':
      case 'password':
      case 'register':
      case NULL:
        // Redirect user to login menu callback.
        // Calling drupal_access_denied() here leads to unexpected results.
        drupal_goto('login');
        break;
    }
  }
}

/**
 * Implements hook_menu().
 */
function no_login_menu() {

  $items = array();
  $items['login'] = array(
    'title' => 'No Login Callback',
    'page callback' => 'no_login_callback',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * No Login callback function for outputting the access denied page.
 */
function no_login_callback() {
  drupal_access_denied();
}

/**
 * Implements hook_block_info_alter().
 */
function no_login_block_info_alter(&$blocks, $theme, $code_blocks) {
  // Disable the user login block.
  if (isset($blocks['user'])) {
    $blocks['user']['login']['status'] = 0;
    $blocks['user']['login']['region'] = -1;
  }
}

/**
 * Implements hook_form_alter().
 */
function no_login_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_login_block':
      $form['#validate'][] = 'no_login_form_validate';
      break;
  }
  return $form;
}

/**
 * Custom validation function to deny login.
 */
function no_login_form_validate($form, &$form_state) {
  form_set_error('', 'Login is not allowed.');
}
