No Login

This is a module that denies login, registration and password reset to all
users. Once this module is enabled, the only way to login to your site is to
use the 'drush uli' command.

Why would you use this module? The main use case for this module is to enable
it on production environments only, where you consider your production
environment untouchable except in emergency situations.

This module also attempts to disable the user login block (if using core block
placement methods), and intercepts attempts to login via the block and denies
access.
